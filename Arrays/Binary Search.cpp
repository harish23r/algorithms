#include <iostream>
#include <vector>
using namespace std;

int linear_search(int* a, int m, int n, int key) {
	int i;
	for(i=m; i<=n; i++)
		if(a[i] == key)
			return i;
	return -1;
}

int binary_search(int* a, int m, int n, int key) {
	if(n < m)
		return -1;
	int mid = (m+n)/2;
	if(key < a[mid]) 
		return binary_search(a, m, mid-1, key);
	else if(key > a[mid])
		return binary_search(a, mid+1, n, key);
	// else mid is the index of the element 
	else
		return mid;
}

int find_first(int* a, int m, int n, int key) {
	if(n < m)
		return -1;
	int mid = (m+n)/2;
	if((key == a[mid]) &&  (mid == 0 || a[mid] > a[mid-1]))
		return mid;
	else if(key <= a[mid]) 
		return find_first(a, m, mid-1, key);
	else
		return find_first(a, mid+1, n, key);
}

int find_last(int* a, int m, int n, int key) {
	if(m > n)
		return -1;
	int mid = (m+n)/2;
	if((a[mid] == key) && ((mid == n) || (a[mid] < a[mid+1])))
		return mid;
	if(key >= a[mid])
		return find_last(a, mid+1, n, key);
	else
		return find_last(a, m, mid-1, key);
}

int main() {
	int a[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
	int b[] = {40, 40, 40, 40, 60, 65, 70, 70, 70, 90, 90};
	int c[] = {10};
	// cout << binary_search(a, 0, 9, 8) << endl;
	// cout << binary_search(a, 0, 9, 11) << endl;
	cout << binary_search(b, 0, 10, 70) << endl;
	cout << binary_search(b, 0, 10, 40) << endl;
	cout << find_first(b, 0, 10, 70) << endl;
	cout << find_first(b, 0, 10, 40) << endl;
	cout << find_first(b, 0, 10, 90) << endl;
	cout << find_last(b, 0, 10, 70) << endl;
	cout << find_last(b, 0, 10, 40) << endl;
	cout << find_last(b, 0, 10, 90) << endl;
	cout << (char)97 << endl;
	cout << (char)122 << endl;
	// cout << binary_search(b, 0, 8, 10) << endl;
	// cout << binary_search(c, 0, 0, 10) << endl;
	return 0;
}