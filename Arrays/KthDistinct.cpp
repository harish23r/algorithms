#include <iostream>
using namespace std;

int findKthLargest(vector<int>& nums, int k) {
    int len = nums.size();
    int i, count = 1, prev = nums[len-1];
    sort(nums.begin(), nums.begin()+len);
    for(i=len-1; i>=0; i--)  {
    	if(nums[i] != prev) {
    		count++;
    		prev = nums[i];
    	}
    	if(count == k)
    		return nums[i];
    }
   	// Not found 
    if(count < k)
    	return -1;
}

int main() {
    return 0;
}