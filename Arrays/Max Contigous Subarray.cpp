#include <iostream>
#include <vector>
#include <climits>
using namespace std;

int max_subarray(vector<int> nums, int m, int n) {
	if(m == n)
		return nums[m];
	int mid = (m+n)/2;
	int allsum = 0, left_sum = INT_MIN, right_sum = INT_MIN;
	for(int i=m; i<=mid-1; i++) {
		allsum = allsum + nums[i];
		if(allsum > left_sum)
			left_sum += nums[i];
	}
	for(int i=mid; i<=n; i++){
		allsum = allsum + nums[i];
		if(allsum > right_sum) 
			right_sum += nums[i];
	}
	allsum = left_sum + right_sum;
	return max(max(max_subarray(nums, m, mid-1), max_subarray(nums, mid, n)), allsum);
}

int main() {
	vector<int> nums;
	nums.push_back(-2);
	nums.push_back(-3);
	nums.push_back(4);
	nums.push_back(-1);
	nums.push_back(-2);
	nums.push_back(1);
	nums.push_back(5);
	nums.push_back(-3);
	cout << max_subarray(nums, 0, nums.size()-1) << endl;	
	return 0;
}