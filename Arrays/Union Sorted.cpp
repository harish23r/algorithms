#include <iostream>
#include <vector>
using namespace std;

/*	By using the range of elements known 
	Time Complexity: O(range)
   	Space complexity: O(1) */
/*vector<int> union_sorted(int* a, int m, int* b, int n) {
	// Find range, by looking at least & greatest value in both arrays
	int a_max = a[m-1], a_min = a[0];
	int b_max = b[n-1], b_min = b[0];
	int min = a_min < b_min ? a_min : b_min;
	int max = a_max > b_max ? a_max : b_max;
	// cout << min << " " << max << endl;
	vector<int> res;
	int i=0, j=0, k; 
	for(k=min; k<=max; k++) {
		if(a[i] == k || b[j] == k)
			res.push_back(k);
		while(a[i] <= k && i<m) i++;
		while(b[j] <= k && j<n) j++;
	}
	return res;
} */

vector<int> union_sorted(int* a, int m, int* b, int n) {
	vector<int> res;
	int i=0, j=0, temp;
	while(i < m && j < n) {
		if(a[i] <= b[j]) {
			res.push_back(a[i]);
			temp = a[i++];
		} else {
			res.push_back(b[j]);
			temp = b[j++];
		}
		while(a[i] <= temp) i++;
		while(b[j] <= temp) j++;
	}
	while(i < m)
		res.push_back(a[i++]);
	while(j < n)
		res.push_back(b[j++]);
	return res;
}

int main() {
	int a[] = {1,1,3,3};
	int b[] = {1,1,2,2};
	vector<int> res = union_sorted(a, 3, b, 3);
	for(int i=0; i<res.size(); i++) 
		cout << res[i] << " ";
	cout << endl;
	return 0;
}