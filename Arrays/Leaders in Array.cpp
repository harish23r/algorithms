#include <iostream>
using namespace std;
// Complexity O(n)
void find_leaders(int* a, int n) {
	int i, max = a[n];
	for(i = n-1; i >= 0; i--) {
		if(a[i] > max) {
			cout << a[i] << " ";
			max = a[i];
		}
	}
	cout << endl;
}

int main() {
	int a[] = {16, 17, 4, 3, 5, 2};
	find_leaders(a, 6);
	return 0;
}