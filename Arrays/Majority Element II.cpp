class Solution {
public:
    int find_last(vector<int> a, int m, int n, int key) {
	if(m > n)
		return -1;
	int mid = (m+n)/2;
	if((a[mid] == key) && ((mid == n) || (a[mid] < a[mid+1])))
		return mid;
	if(key >= a[mid])
		return find_last(a, mid+1, n, key);
	else
		return find_last(a, m, mid-1, key);
}

    vector<int> majorityElement(vector<int>& nums) {
        int len = nums.size();
        vector<int> res; 
        if(len == 2) {
            cout << "Entering if" << endl;
            if(nums[0] == nums[1]) {
                res.push_back(nums[0]);
                return res;
            }
            return nums;
        
        sort(nums.begin(), nums.begin()+len);
        int i;
        for(int j=0; j<len; j++) cout << nums[j] << " ";
        cout << endl;
        int prev;
        for(i=0; i<len; i++) {
            cout << nums[i] << " ";
            int last = find_last(nums, 0, len-1, nums[i]);
            cout << last << " " <<  last-i+1 << endl;
            if((last >= 1) && ((last-i+1) > len/3))
                res.push_back(nums[i]);
            // prev = nums[i];
            // while(nums[++i] == prev);
        }
        return res;
    }
};