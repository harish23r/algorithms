#include <iostream>
#include <vector>
using namespace std;

int minJumps(vector<int> dist) {
	int len = dist.size();
	if(len <= 1)
		return 0;
	if(dist[0] == 0)
		return 0;
	int i=len-2;
	int min = 0, steps;
	while(i >= 0) {
		steps = 1;
		while(i >= 0 && dist[i] - steps > 0) {
			i--;
			steps++;
		}
		cout << i << " " << dist[i] << " " << steps << endl; 
		min++;
		i--; 
	}	
	return min; 
}

int main() {
	vector<int> dist;
	dist.push_back(1);
	dist.push_back(3);
	dist.push_back(5);
	dist.push_back(8);
	dist.push_back(9);
	dist.push_back(2);
	dist.push_back(3);
	dist.push_back(2);
	dist.push_back(4);
	dist.push_back(8);
	dist.push_back(9);
	cout << minJumps(dist) << endl;
	return 0;
}