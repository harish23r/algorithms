#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int findKthLargest(vector<int> nums, int k) {
    int len = nums.size();
    int i, count = 0, prev = nums[len-1];
    sort(nums.begin(), nums.begin()+len);
    for(i=len-1; i>=0; i--)  {
    	if(nums[i] != prev) {
    		count++;
    		prev = nums[i];
    	}
    	if(count == k)
    		return nums[i];
    }
   	// Not found 
    if(count < k)
    	return -1;
}

int findKthSmallest(vector<int> nums, int k) {
    int len = nums.size();
    int i, count = 0, prev = nums[0];
    sort(nums.begin(), nums.begin()+len);
    for(i=0; i<len; i++)  {
    	if(nums[i] != prev) {
    		count++;
    		prev = nums[i];
    	}
    	if(count == k)
    		return nums[i];
    }
   	// Not found 
    if(count < k)
    	return -1;
}

int main() {
	vector<int> nums;
	nums.push_back(4);
	nums.push_back(4);
	nums.push_back(8);
	nums.push_back(9);
	nums.push_back(9);
	nums.push_back(7);
	nums.push_back(4);
	nums.push_back(7);
	nums.push_back(8);	
	cout << findKthLargest(nums, 4) << endl;
	return 0;
}