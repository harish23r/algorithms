#include <iostream>
#include <string>
#include <cstdlib>
#include <climits>
using namespace std;

/*
 * Function to reverse an array from index m to index n inclusive
 * a : array, m : start index, n : end index
 * Return value: none
 */
int reverse(int* a, int m, int n) {
	for(int i=m; i < (n+m)/2+1; i++) 
		swap(a[i], a[n-i]);
}

/*
 * Function: 
 * Arguments:
 * Return value:
*/
int findMinIndex(int* a, int m, int n) {
	int min = a[m];
	int min_index = m; 
	for(int i=m; i<=n; i++)
		if(a[i] < min) {
			min = a[i];
			min_index = i;
		}
	return min_index;
}

/*
 * Function: 
 * Arguments:
 * Return value:
*/
int findMin(int* a, int m, int n) {
	int min = a[m];
	for(int i=m; i<=n; i++)
		if(a[i] < min)
			min = a[i];
	return min;
}

/*
 * Function: 
 * Arguments:
 * Return value:
*/
int findMaxIndex(int* a, int m, int n) {
	int max = a[m];
	int max_index = m; 
	for(int i=m; i<=n; i++)
		if(a[i] > max) {
			max = a[i];
			max_index = i;
		}
	return max_index;
}

/*
 * Function: to find maximum value in array between a range
 * Arguments: a: array, m: range min, n: range max
 * Return value: maximum value
*/
int findMax(int* a, int m, int n) {
	int max = a[m];
	for(int i=m; i<=n; i++) 
		if(a[i] > max)
			max = a[i];
	return max;
}

void alternate_sort(int* a, int n) {
	for(int i=0; i<n; i++) {
		if(i%2 == 0)
			swap(a[i], a[findMaxIndex(a, i, n-1)]);
		else
			swap(a[i], a[findMinIndex(a, i, n-1)]);
	}
}

/*
 * Function: to print all elements 
 * Arguments: a: array, n: number of elements
 * Return value: none
*/
void print(int* a, int n) {
	for(int i=0; i<n; i++)
		cout << a[i] << " ";
	cout << endl;
}

/*
 * Function: to print all elements within given indices inclusive
 * Arguments: a: array, m, n: range of 
 * Return value: none
*/
void print(int* a, int m, int n) {
	for(int i = m; i <= n; i++) {
		cout << a[i] << " ";
	}
	cout << endl;
}

int main()
{
	int a[] = {1, 2, 3, 4, 5, 6, 7};
	alternate_sort(a, sizeof(a)/sizeof(int));
	print(a, sizeof(a)/sizeof(int));
	reverse(a, 0, sizeof(a)/sizeof(int)-1);
	print(a, sizeof(a)/sizeof(int));
	return 0;
}