#include <iostream>
#include <vector>
using namespace std;

int lcs_recursive(string a, int n, string b, int m) {
	if(n == -1 || m == -1)
		return 0;
	if(a[n] == b[m])
		return lcs_recursive(a, n-1, b, m-1) + 1;
	else
		return max(lcs_recursive(a, n-1, b, m), lcs_recursive(a, n, b, m-1));
}

string lcs(string a, string b) {
	string res;
	int a_len = a.size();
	int b_len = b.size();
	int dp[a_len+1][b_len+1];
	for(int i=0; i<=a_len; i++)
		dp[i][0] = 0;
	for(int i=0; i<=b_len; i++)
		dp[0][i] = 0;
	for(int i=1; i<=a_len; i++) 
		for(int j=1; j<=b_len; j++)
			if(a[i-1] == b[j-1]) 
				dp[i][j] = dp[i-1][j-1] + 1;
			else
				dp[i][j] = max(dp[i-1][j], dp[i][j-1]);
	
	for(int i=0; i<=a_len; i++)  {
		for(int j=0; j<=b_len; j++)
			cout << dp[i][j] << " ";
		cout << endl;
	}

	int i = a_len;
	int j = b_len;
	while(i > 0 && j > 0) {
		if(a[i-1] == b[j-1]) {
			res.insert(0, 1, a[i-1]);
			i--; j--;
		}
		else if(dp[i][j-1] <= dp[i-1][j])
			i--;
		else
			j--;
	}
	return res;
}

int main() {
	string a = "ABCDGH";
	string b = "AEDFHR";
	cout << lcs_recursive(a, a.size()-1, b, b.size()-1) << endl;
	cout << lcs(a, b) << endl;
	return 0;
}