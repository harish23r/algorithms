#include <iostream>
using namespace std;

int edit_distance(string a, string b) {
	int alen = a.size();
	int blen = b.size();
	int dp[alen+1][blen+1];
	for(int i=0; i<=alen; i++)
		dp[i][0] = i;
	for(int i=0; i<=blen; i++)
		dp[0][i] = i;
	for(int i=1; i<=alen; i++) {
		for(int j=1; j<=blen; j++) {
			int min_cost; 
			if(a[i-1] == b[j-1])
				min_cost = dp[i-1][j-1];
			else if(a[i-1] != b[j-1])
				min_cost  = dp[i-1][j-1] + 1;
			if(min(dp[i][j-1], dp[i-1][j]) + 1 < min_cost)
					min_cost = min(dp[i][j-1], dp[i-1][j]) + 1;
			dp[i][j] = min_cost;
		}
	}

	for(int i=0; i<=alen; i++) {
		for(int j=0; j<=blen; j++)
			cout << dp[i][j] << " ";
		cout << endl;
	}
	return dp[alen][blen];
}

int main() {
	string a = "abc";
	string b = "b";
	cout << edit_distance(a, b) << endl;
	return 0;
}