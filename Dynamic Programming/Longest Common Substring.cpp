#include <iostream>
using namespace std;

// Longest Palindromic Substring
string LPS(string s) {
	int len = s.size();
	if(len < 2) 
		return s;
	int max_diff = 0, start, end;
	int dp[len][len] = {{0}};
	// Each character is palindrome
	for(int i=0; i<len; i++)
		dp[i][i] = 1;
	for(int i=0; i<len-1; i++) {
		 dp[i][i+1] = (s[i] == s[i+1] ? 1 : 0);
		 if(dp[i][i+1] && 1 > max_diff) {
		 	max_diff = 1;
		 	start = i; end = i + 1;
		 }
	}
	// For substrings of length more than 3
	for(int k=2; k<len; k++)
		for(int i=0; i<len-k; i++) {
			int j = i + k;
			if(dp[i+1][j-1] && s[i] == s[j]) {
				dp[i][j] = 1;
				if(j - i > max_diff) {
					max_diff = j-i;
					start = i; end = j;
				}
			}
			else
				dp[i][j] = 0;
		}

	string lps;
	if(max_diff == 0)
		lps = s.substr(0, 1);
	else if(max_diff == 1)
		lps = s.substr(start, 2);
	else
		lps = s.substr(start, max_diff+1);
	return lps;
}

int main() {
	string s = "abac";
	cout << LPS(s) << endl;
	return 0;
}

/*#include <iostream>
using namespace std;

// Longest Palindromic Substring
string LPS(string s) {
	int len = s.size();
	if(len < 1) 
		return s;
	int dp[len][len] = {{0}};
	// Each character is palindrome
	for(int i=0; i<len; i++)
		dp[i][i] = 1;
	for(int i=0; i<len-1; i++)
		 dp[i][i+1] = (s[i] == s[i+1] ? 1 : 0);
	// For substrings of length more than 3
	for(int k=2; k<len; k++)
		for(int i=0; i<len-k; i++) {
			int j = i + k;
			if(dp[i+1][j-1] && s[i] == s[j]) 
				dp[i][j] = 1;
			else
				dp[i][j] = 0;
		}

	int start, end;
	int i = len-1, j = len-1;
	while(--i >= 0 && dp[i][j] != 1);
	end = i;
	while(i < len-1) {
		i++; j--;
	}
	start = j;
	cout << start << endl;
	string lps;
	return lps;
}

int main() {
	string s = "caba";
	cout << LPS(s) << endl;
	return 0;
}*/