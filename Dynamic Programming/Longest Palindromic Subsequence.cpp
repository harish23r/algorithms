#include <iostream>
#define CROSS 1
#define LEFT 2
#define DOWN 3
using namespace std;

/*struct store {
	int len;	// Length of subsequence until now
	int dir;	// Direction to move to retreive string
};

string LPS(string s) {
	int len = s.size();
	store dp[len][len];
	for(int i=0; i<len; i++) {
		for(int j=0; j<len; j++)
			dp[i][j].len = 0;
	}
	// All single characters are palindromes
	for(int i=0; i<len; i++)
		dp[i][i].len = 1;
	// For length 2: 2 consecutive same elements
	for(int i=0; i<len-1; i++) 
		if(s[i] == s[i+1])
			dp[i][i+1].len = 2;
		else
			dp[i][i+1].len = 1;
	// Subsequences for length 3 and more
	for(int k=2; k<len; k++) {
		for(int i=0; i<len-k; i++) {
			int j = i + k;
			if(s[i] == s[i+j]) {
				dp[i][j].len = dp[i+1][j-1] + 2;
				dp[i][j].dir = CROSS;
			}
			else {
				dp[i][j].len = max(dp[i+1][j] > dp[i][j-1]);
				dp[i][j] = (dp[i+1][j] > dp[i][j-1] ? DOWN : LEFT); 
			}
		}
	}

}*/

int LPSLength(string s) {
// string LPS(string s) {
	int len = s.size();
	int dp[len][len];
	for(int i=0; i<len; i++) {
		for(int j=0; j<len; j++)
			dp[i][j] = 0;
	}
	// All single characters are palindromes
	for(int i=0; i<len; i++)
		dp[i][i] = 1;
	// For length 2 to n consecutive same elements
	for(int i=0; i<len-1; i++) 
		if(s[i] == s[i+1])
			dp[i][i+1] = 2;
		else
			dp[i][i+1] = 1;
	// Subsequences for length 3 and more
	for(int k=2; k<len; k++) {
		for(int i=0; i<len-k; i++) {
			int j = i + k;
			if(s[i] == s[j]) {
				dp[i][j] = dp[i+1][j-1] + 2;
			}
			else {
				dp[i][j] = max(dp[i+1][j], dp[i][j-1]);
			}
		}
	}
	for(int i=0; i<len; i++) {
		for(int j=0; j<len; j++)
			cout << dp[i][j] << " ";
		cout << endl;
	}
	return dp[0][len-1];
	// string res;
	// return res;
}

int main() {
	string s = "AABCDEBAZ";
	cout << LPSLength(s) << endl; 
	return 0;
}