// Generate all combinations of set of numbers (non repeating elements)
#include <iostream>
#include <vector>
using namespace std;

void generate_combinations(vector<int> a, bool* inComb, int k, int n, vector<vector<int> >& res) {	
	// Base Case: Found a combination
	cout << "k = " << k << endl;
	if(k == n-1) {
		vector<int> comb;
		// 
		for(int i=0; i<n; i++)
			if(inComb[i] == true)
				comb.push_back(a[i]);
		res.push_back(comb);
	}
	// Combination not yet found
	else {
		k = k + 1;
		// Recursively generate combinations when kth item is present in the combination
		inComb[k] = true;
		generate_combinations(a, inComb, k, n, res);
		// Recursively generate combinations when kth item is not present in the combination
		inComb[k] = false;
		generate_combinations(a, inComb, k, n, res);
	}
}

vector<vector<int> > combinations(vector<int> a, int n) {
	vector<vector<int> > res;
	bool inComb[n];
	generate_combinations(a, inComb, -1, n, res);
	return res; 
}

int main() {
	vector<int> nums;
	nums.push_back(1);
	nums.push_back(2);
	nums.push_back(3);
	nums.push_back(4);
	nums.push_back(5);

	vector<vector<int> > res = combinations(nums, nums.size());
	for(int i = 0; i < res.size(); i++) {
		vector<int> row = res[i];
		for(int j = 0; j < row.size(); j++)
			cout << row[j] << " ";
		cout << endl;
	}
	return 0;
}