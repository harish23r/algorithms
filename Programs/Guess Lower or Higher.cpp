#include <iostream>
using namespace std;

class Solution {
public:
	int x;
	Solution(int num) {
		x = num;
	}

	int guess(int num) {
		if(x < num)
			return -1;
		if(x > num)
			return 1;
		return 0;
	}

    long int findGuess(long int l, long int r) {
        long int mid = (l+r)/2;
        if(guess(mid) == 0)
            return mid;
        else if(guess(mid) == -1)
            return findGuess(l, mid-1);
        else if(guess(mid) == 1)
            return findGuess(mid+1, r);
    }

    long int guessNumber(long int n) {
       return findGuess(1, n);
    }
};

int main() {
	Solution S(1702766719);
	cout << S.guessNumber(2126753390) << endl;
	return 0;
}