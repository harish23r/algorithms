#include <iostream>
using namespace std;

long long int factorial(long long int n) {
	long long int i = 1;
	for(int j=1; j<=n; j++)
		i = i * j;
	return i; 
}

int main() {
	cout << "Program Start" << endl;
	for(long long int n=1; n<=15; n++)
		cout << factorial(n) << endl;
	return 0;
}