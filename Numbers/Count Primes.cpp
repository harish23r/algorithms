#include <iostream>
#include <climits>
#include <cmath>
using namespace std;

int countPrimes(long int n) {
    // Allocate prime array, 1 = prime, 0 = not prime
    long int primes[n+1];
    // Initially set all numbers as prime
    for(long int i=2; i<n; i++)
        primes[i] = 1;
    // From 2 to sqrt(n),..
    for(long int i=2; i*i < (n); i++)
        // If number isp rime,..
        if(primes[i] == 1)
            // Mark all its factors non primes
            for(long int j=2, k=i*j; k < n; j++, k=i*j)
                primes[k] = 0;
    // Count number having one as index
    long int count = 0;
    for(long int i=2; i<n; i++)
        if(primes[i] == 1)
            count++;
    // Return count
    return count;
}

int main() {
    cout << INT_MAX << " " << LONG_MAX << endl;
    long int n = 1500000;
    cout << n << endl;
    cout << countPrimes(n) << endl;
    return 0;
}