/*
Counting sort CPP
Works only for positive numbers
*/
#include <iostream>
#define RANGE_MIN 0
#define RANGE_MAX 100
using namespace std;

void counting_sort(int* a, int n) {
	int count[RANGE_MAX+1] = {0};
	int i, j, k;
	for(i=0; i<n; i++)
		++count[a[i]];
	for(i=0, k=0; i<RANGE_MAX+1; i++) {
		for(j=0; j<count[i]; j++) {
			a[k++] = i; 
		}
	}
}

int main() 
{
	int a[] = {23, 67, 12, 45, 98, 34, 16};
	counting_sort(a, 7);
	for(int i=0; i<7; i++) 
		cout << a[i] << " ";
	cout << endl;
	return 0;
}