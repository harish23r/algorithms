#include <iostream>
using namespace std;

void swap(int* a, int* b) {
	int temp = *a;
	*a = *b;
	*b = temp;
}

void bubble_sort(int* a, int n) {
	int i, j;
	for(i=0; i<n; i++)
		for(j=i+1; j<n; j++)
			if(a[i] > a[j])
				swap(&a[i], &a[j]); 
}