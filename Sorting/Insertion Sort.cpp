#include <iostream>
using namespace std;

void insertion_sort(int* a, int n) {
	int i, j, temp;
	for(i=1; i<n; i++) {
		j = i-1;
		temp = a[i];
		while(j > 0 && a[j-1] > a[j]) {
			a[j] = a[j-1];
			--j;
		}
		a[j] = temp;
	}
}

int main() {

}