#include <iostream>
using namespace std;

int find_min_index(int* a, int m, int n) {
	int i;
	int min = a[m], min_index = m;
	for(i=m; i<n; i++)
		if(a[i] < min) {
			min_index = i;
			min = a[i]; 
		}
	return min_index;
}

void selection_sort(int* a, int n) {
	int i, min_index;
	for(i=0; i<n; i++) {
		min_index = find_min_index(a, i, n);
		swap(a[min_index], a[i]);
	}
}

int main() {
	return 0;
}