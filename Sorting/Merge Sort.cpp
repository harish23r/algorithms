#include <iostream>
using namespace std;

void merge(int* a, int* L, int l, int* R, int r) {
	int i=0, j=0, k=0;
	while(i < l && j < r) 		
		if(L[i] < R[j])
			a[k++] = L[i++];
		else
			a[k++] = R[j++];
	while(i < l) 
		a[k++] = L[i++];
	while(j < r)
		a[k++] = R[j++];
}

void mergesort(int* a, int n) { 
	if(n < 2) 
		return;
	int mid = n/2, i;
	int L[mid], R[n-mid];
	for(i=0;i<mid; i++)
		L[i] = a[i];
	for(i=mid; i<n; i++)
		R[i-mid] = a[i];
	mergesort(L, mid);
	mergesort(R, n-mid);
	merge(a, L, mid, R, n-mid);
}

int main() {
	int a[] = {3,2,4,7,9,6,5,1};
	mergesort(a, 7);
	for(int i=0; i<7; i++) 
		cout << a[i] << " ";
	cout << endl;
	return 0;
}
