#include <iostream>
#include <climits>
#include <cctype>
using namespace std;

bool isDigit(char c) {
	return (c >=48 && c <= 57) ? true : false;
}

int StrToInt(string s) {
	int res = 0, i=0;
	int len = s.size();
	int sign = 0;
	while(!isdigit(s[i]) && s[i] != '-' && s[i] != '+' && i < len)
		i++;
	if(i >= len)
	    return res;
	if(s[i] == '-') {
	   sign = 1;
	   i++;
	}
	else if(s[i] == '+') {
	    i++;
	}
	while(i<len && isdigit(s[i])) {
	    int diff = int(s[i] - 48);
	    if(diff > INT_MAX - res) {
	        if(sign)
	            return INT_MIN;
	        return INT_MAX;
	    }
	       
		res = (res*10) + diff;
		i++;
		if(!isdigit(s[i]))
		    break;
	}
	if(sign) {
	    res = res - 2 * res;
	}
	return res;
}

int main() {
	string s = " -123  df ";
	int x = 20;
	cout << INT_MAX << " " << INT_MIN << endl;
	cout << StrToInt(s) << endl;
	return 0;
}	