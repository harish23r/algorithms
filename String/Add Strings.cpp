#include <iostream>
using namespace std;

string addStrings(string num1, string num2) {
    int len1 = num1.size(), len2 = num2.size();
    int i = len1-1, j = len2-1;
    int sum, val, carry = 0;
    string res;
    while(i >= 0 && j >= 0) {
        cout << i << " " << j << endl;
        cout << num1[i] - 48 << " " << num2[j] - 48 << " " << carry << endl;
        sum = ((num1[i] - 48 ) + (num2[j] - 48) + carry);
        val = sum%10;
        carry = sum/10;
        cout << val << " " << carry << endl;
        res.push_back(val + 48);
        cout << res << endl;
        i--; j--;
    }
    while(i >= 0) {
        sum = (num1[i] - 48 ) + carry;
        val = sum%10;
        carry = sum/10;
        res.push_back(val + 48);
        i--;
    }
    while(j >= 0) {
        sum = (num2[j] - 48) + carry;
        val = sum%10;
        carry = sum/10;
        res.push_back(val + 48);
        j--;
    }
    if(carry) {
        cout << "yes carry: " << carry << endl;
        res.push_back(carry + 48);
        cout << res << endl;
    }
    for(i = 0, j = res.size()-1; i < j; i++, j--)
        swap(res[i], res[j]);
    return res;
}

int main() {   
    string a = "123", b = "123";
    cout << addStrings(a, b);
    return 0;
}